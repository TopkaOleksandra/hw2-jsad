//Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.
//Використовуємо коли є вірогідність помилки та необхідно передбачити продовження виконання коду,
// наприклад коли іде обробка даних від користувачів, або запити на сервер


const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

const root = document.querySelector("#root");
const ul = document.createElement("ul")
books.forEach (book=> {
    try {
        if (!book.hasOwnProperty("author")) {
            throw new Error(`Немає автора: ${JSON.stringify(book)}`);
        }
        if (!book.hasOwnProperty("name")) {
            throw new Error(`Немає назви: ${JSON.stringify(book)}`);
        }
        if (!book.hasOwnProperty("price")) {
            throw new Error(`Немає ціни:  ${JSON.stringify(book)}`);
        }

        let li = document.createElement("li");
        li.textContent = `${book.author}: ${book.name}, ${book.price}`;
        ul.appendChild(li);
    } catch (error) {
        console.log(error);
    }
})


root.appendChild(ul);



